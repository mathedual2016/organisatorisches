Explanation: Leaves out the whitespace between Destination and time to destination.

Teams:
Break_this -- throws an uncaught error.
no_more_names -- zero size big integer error, doesn't write an output file
wettbewerb-16 -- all Container stay in Aachen
mathedual-wettbewerb-16 -- Container stay in Aachen. Does not find the ship destination
