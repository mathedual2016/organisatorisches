Explanation: What happens if the IDs are not unique? I expect an error because it does not make sense to have more than one container per ID.

Teams:
programming-contest -- parsed as usual but didn't show containers with multiple ids.
break_this -- parsed as usual, shows up the multiple ids with destinations
wettbewerb-16 -- all container stay in Aachen
