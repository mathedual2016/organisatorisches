# Fragen Basisaufgabe

**Muss eine ID eindeutig sein?**
*Ja.*

**Was soll in der Ausgabedatei stehen, wenn ein Container nicht einem 
Schiff zugeteilt werden konnte?**
*Der Standort des Containers, also Aachen.*

**Wenn ein Container keinem Schiff zugeordnet werden kann, wo taucht er 
in der Ausgabedatei auf?**
*In der Liste der Container.*

**Kann davon ausgegangen, dass die Identifikationen in der Eingabedatei in der richtigen Reihenfolge sind?**
*Nein.*

**Kann davon ausgegangen werden, dass die Identifikationen Integer sind?**
*Nein.*

**Wie werden Trennzeichen und andere nicht darstellbare Zeichen in den Eingabedateien behandelt**
*Es gibt keine Escapesequenzen.*

**Müssen String Identifikatoren akzeptiert werden?**
*Ja.*

**Wie müssen Identifikatoren in der Ausgabedatei sortiert werden?**
*Alphabetisch.*

**Müssen die Eigenschaften der Schiffe und Container immer in der selben Reihenfolge wie im Beispiel sein?**
*Ja.*

**Kann bei den Kapazitäten davon ausgegangen werden, dass es positive Ganzzahlen sind?**
*Um ein sinnvolles Ergebnis zu produzieren: Ja.*

**Wenn ein User totalen Bulls... in einer Zeile eingibt soll das Programm komplett abbrechen, oder nur die Zeile ignorieren?**
*TOTALER BULLS... (Syntaxfehler) = abbrechen.*

---

# Fragen Erweiterung 1

**Was ist ein Syntaxfehler?**
*<https://de.wikipedia.org/wiki/Syntaxfehler>*

**Muss das Programm mit beiden Eingabeformaten umgehen können?**
Nein. Das Programm muss nur mit dem letzten Format umgehen können.

---

# Fragen Erweiterung 2
**Werden Schiffe, welche direkt den Zielhafen des Containers anlaufen, bevorzugt vor Schiffen, welche andere Häfen auf dem Weg anlaufen?**
*Nein.*

---

# Fragen Erweiterung 3
** Was passiert bei einem Programmabbruch?**
*Es wird in jedem Fall eine Ausgabedatei erzeugt. Diese enthält entweder ein gültiges Ergebnis oder eine sprechende Fehlermeldung nach dem Format* 
```
!!! "MELDUNG" !!!
```

**Ist ein Schiff ohne Zielhafen ein legitimes Schiff?**
*Nein, siehe Aufgabenstellung*

**Sind Timestamps ganzzahlig?**
*Ja.*

**Ist das Leerzeichen zwischen einem Hafen und der angegebenen Reisedauer immer vorhanden?**
*Nein, maßgeblich ist das letzte Paar von Klammern.*

**Was passiert, wenn die Ausgabedatei nicht geschrieben werden kann?**
*Die Ausgabe wird auf den stdout umgeleitet.*

**Wie soll reagiert werden, wenn Datentypen durch eine entsprechende Eingabedatei überlaufen?**
*Programmabbruch.*

**Ist das Leerzeichen zwischen einem Hafen und der angegebenen Reisedauer, wenn vorhanden, Teil des Namens des Hafens?**
*Nein.*

**Wird die Meldung bei einem Programmabbruch gedifft?**
*Nein.*

**Wie groß darf die Fehlermeldung bei einem Programmabbruch werden?**
*Eine Ausgabedatei, welche eine Fehlermeldung enthält, darf maximal 100 KB groß sein. Jedes weitere Kilobyte ist in Schokoriegeln an die Organisatoren auszuzahlen.*

**Werden die Häfen in der Reihenfolge angefahren, in der sie in der Eingabedatei stehen**
*Ja.*

**Darf außer im o.g. Fehlerfall auf den standard out geschrieben werden**
*Nein das Programm ezeugt sonst _keinerlei_ Ausgabe.*

**Muss TTS eingesetzt werden um die Fehlermeldungen zum Sprechen zu bringen**
*Nein. Es handelt sich um einen Anthropomorphismus.*

**Dürfen Dateien durch das Programm auf "read only" gesetzt werden?**
*Nein.*